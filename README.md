# OD Application POC

Application to run TF2 inference on RaspberryPi equipped with camera module. Code is based on the following sources:

Birodkar V., Rathod V., Joglekar S. et al. (2020) Object Detection API with TensorFlow 2: Installation. Published on GitHub.com in TensorFlow repository [online] https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2.md#installation [12.07.2021]

Rathod V., Wu N., Jinks E., Chow D., et al., (2020 A) Preparing Inputs. Published on GitHub.com in TensorFlow repository [online] https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/preparing_inputs.md [07.10.2021]

Rathod V., Huang J. (2020 B) TensorFlow 2 meets the Object Detection API. Published on TensorFlow Blog [online] https://blog.tensorflow.org/2020/07/tensorflow-2-meets-object-detection-api.html [06.10.2021]

Rathod V., TensorFlow Gardner et al. (2021) Training and Evaluation with TF2. Published on GitHub.com in TensorFlow repository [online] https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_training_and_evaluation.md#recommended-directory-structure-for-training-and-evaluation [19.09.2021]

Main S., Rathod V., Wu N., et al. (2021) Configuring the Object Detection Training Pipeline. Published on GitHub.com in TensorFlow repository [online] https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/configuring_jobs.md [21.10.2021]

Solawetz J. (2020 A) Roboflow-TensorFlow2-Object-Detection.ipynb. Google Colab notebook available at: https://colab.research.google.com/drive/1sLqFKVV94wm-lglFq_0kGo2ciM0kecWD [13.09.2021]

TensorFlow Guide (2021) TensorFlow Lite inference. Published on tensorflow.org [online]
https://www.tensorflow.org/lite/guide/inference [02.09.2021]

TensorFlow Guide (2021 A) Official TF2 guide for object detection tutorial: officialTFguide.ipynb. Google Colab notebook available at: https://colab.research.google.com/drive/1CmcrywhKJI5BQ69RN28YvS_ypvvMdQlF [13.09.2021]




In order to test the application train an own model and convert it to TFLite or use the one provided in this repo (ssd_mobilenet_v2_fpnlite). This application will use the model to detect torx screws in the FOW of the camera, calcualte their poses (class "correct", "upwards", or "sideway"), and coordinates. The results are stored to a txt file. Parameters for inference can be set in the file run_app.py
camera resoultion, input mean, input std, image format, cropping margin, min conf threshold should not be changed. When changing model or using a new camera it is imperative to recalibrate the camera and save the calibration parameters to the "camera_calib_settings" directory and udate the path to it in the script ("PATH_TO_CALIB_SETTINGS"). Furthermore, if a new model is used, the paths to the labels files ("PATH_TO_LABELS") and .tflite graph ("PATH_TO_MODEL_DIR") have to be adapted.

Various models trained throughout the development of my thesis can be downloaded from here: https://drive.google.com/drive/folders/1gDfP52mxyhGrqAeQMgEhdYkEe7jYBiPA?usp=sharing
Note, this application requires a tflite graph! Can be found in the model directories (but not all been converted to tflite) under "fine_tuned_model" in "tflite" directory. "model.tflite" file is the relevant one.

To execute clone this repo to a raspberryPi equipped with a camera and the required libraries. TFLite has to be downloaded, follow this official guide (Install TensorFlow Lite for Python - Linux): https://www.tensorflow.org/lite/guide/python  
Then:
$ pyhton3 od_applocation_poc/run_app.py)

