pip install -q tflite_support
git clone --depth 1 https://github.com/tensorflow/models
cd models/research
protoc object_detection/protos/*.proto --python_out=.
cp object_detection/packages/tf2/setup.py .
pip install -q .
wget http://download.tensorflow.org/models/object_detection/tf2/20200711/ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8.tar.gz
tar -xf ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8.tar.gz
rm ssd_mobilenet_v2_fpnlite_640x640_coco17_tpu-8.tar.gz
git clone https://gitlab.com/FabioMotta1/ssd-mobilenet-v2-fpnlite-640x640_5000steps.git
unzip ./ssd-mobilenet-v2-fpnlite-640x640_5000steps/fine_tuned_model-20210928T115441Z-001.zip -d .
unzip ./ssd-mobilenet-v2-fpnlite-640x640_5000steps/training-20210928T115543Z-001.zip -d .
rm -rf ssd-mobilenet-v2-fpnlite-640x640_5000steps