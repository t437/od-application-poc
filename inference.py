import cv2
import time
import numpy as np

def run_inference(image, input_width, input_height, INPUT_MEAN, INPUT_STD, floating_model, interpreter, input_details, output_details):
    # Acquire frame and resize to expected shape [1xHxWx3]
    image_rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    imH, imW, _ = image.shape
    image_resized = cv2.resize(image_rgb, (input_width, input_height))
    input_data = np.expand_dims(image_resized, axis=0)

    # Normalize pixel values if using a floating model (i.e. if model is non-quantized)
    if floating_model:
        input_data = (np.float32(input_data) - INPUT_MEAN) / INPUT_STD

    # time elapsed for inference
    start = time.time()

    # Perform the actual detection by running the model with the image as input
    interpreter.set_tensor(input_details[0]['index'],input_data)
    interpreter.invoke()

    # Retrieve detection results
    boxes = interpreter.get_tensor(output_details[1]['index'])[0] # Bounding box coordinates of detected objects
    classes = interpreter.get_tensor(output_details[3]['index'])[0] # Class index of detected objects
    scores = interpreter.get_tensor(output_details[0]['index'])[0] # Confidence of detected objects

    end = time.time()
    inference_time = end - start

    return imH, imW, boxes, classes, scores, inference_time