import io
import cv2
import time
import picamera
import numpy as np
import tflite_runtime.interpreter as tflite

import real_xyz
#import inference
from inference import run_inference


PATH_TO_MODEL_DIR = "./fine_tuned_model_ssd_mobilenet_v2_fpnlite_15ksteps_closeup/tflite/model.tflite"
PATH_TO_LABELS = "tflite_label_map.txt"
MIN_INITIAL_CONF_THRESH = 0.2 # use low value to be sure all screws are detected in original images, 
#the coordinates of the objects detected based on this threshold are then used to crop the  image
MIN_CONF_THRESH = 0.8 # thresh of second inference
CROPPING_MARGIN = 5 # in percent, this increases/diminishes the coordinates used to crop in order 
# to add a margin (e.g. 5% of pixels) to the coordinate
PATH_TO_CALIB_SETTINGS = "./camera_calib_settings/"
CAMERA_RESOLUTION_W = 3280
CAMERA_RESOLUTION_H = 2464
IMAGE_CAPTURE_FORMAT = 'jpeg'
INPUT_MEAN = 127.5 # for pixel value normalization
INPUT_STD  = 127.5 # for pixel value normalization
camera_matrix   = np.load(PATH_TO_CALIB_SETTINGS + "cam_mtx.npy")
dist            = np.load(PATH_TO_CALIB_SETTINGS + "dist.npy")
newcameramatrix = np.load(PATH_TO_CALIB_SETTINGS + "newcam_mtx.npy")
roi             = np.load(PATH_TO_CALIB_SETTINGS + "roi.npy")

interpreter = tflite.Interpreter(model_path = PATH_TO_MODEL_DIR)
with open(PATH_TO_LABELS, 'r') as f:
    labels = [line.strip() for line in f.readlines()]

interpreter.allocate_tensors()
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
input_height = input_details[0]['shape'][1]
input_width = input_details[0]['shape'][2]
floating_model = (input_details[0]['dtype'] == np.float32)

stream = io.BytesIO()
with picamera.PiCamera() as camera:
    camera.resolution = (CAMERA_RESOLUTION_W, CAMERA_RESOLUTION_H)
    camera.capture(stream, format=IMAGE_CAPTURE_FORMAT)

buff = np.frombuffer(stream.getvalue(), dtype=np.uint8) # convert the pic into a np array
image = cv2.imdecode(buff, 1)
image_undst = cv2.undistort(image, camera_matrix, dist, None, newcameramatrix) # undistort image
x, y, w, h = roi # crop image according to ROI determined by cv2.getOptimalNewCameraMatrix() [see camera calibration]
undistorted_roi = image_undst[y:y+h, x:x+w]

print('Running inference ... ')
imH, imW, boxes, classes, scores, inference_time = run_inference(undistorted_roi, input_width, input_height, INPUT_MEAN, INPUT_STD, floating_model, interpreter, input_details, output_details)

# find where to crop
crop_coords = {"xmin": w, "ymin": h, "xmax": 0, "ymax": 0}
for i in range(len(scores)):
    if ((scores[i] > MIN_INITIAL_CONF_THRESH) and (scores[i] <= 1.0)):
        # Get bounding box coordinates and draw box
        # Interpreter can return coordinates that are outside of image dimensions, need to force them to be within image using max() and min()
        ymin = int(max(1,(boxes[i][0] * imH)))
        if ymin < crop_coords["ymin"]:
            crop_coords["ymin"] = ymin

        xmin = int(max(1,(boxes[i][1] * imW)))
        if xmin < crop_coords["xmin"]:
            crop_coords["xmin"] = xmin

        ymax = int(min(imH,(boxes[i][2] * imH)))
        if ymax > crop_coords["ymax"]:
            crop_coords["ymax"] = ymax

        xmax = int(min(imW,(boxes[i][3] * imW)))
        if xmax > crop_coords["xmax"]:
            crop_coords["xmax"] = xmax

print("to crop: ", crop_coords)
# add some padding/margin
crop_coords['ymin'] = int(crop_coords['ymin'] * (1 - (CROPPING_MARGIN/100)))
crop_coords['ymax'] = int(crop_coords['ymax'] * (1 + (CROPPING_MARGIN/100)))
crop_coords['xmin'] = int(crop_coords['xmin'] * (1 - (CROPPING_MARGIN/100)))
crop_coords['xmax'] = int(crop_coords['xmax'] * (1 + (CROPPING_MARGIN/100)))
undistorted_roi_cropped = undistorted_roi[crop_coords['ymin']:crop_coords['ymax'], crop_coords['xmin']:crop_coords['xmax']]

print('Running inference on ROI ... ')
imH, imW, boxes, classes, scores, inference_time2 = run_inference(undistorted_roi_cropped, input_width, input_height, INPUT_MEAN, INPUT_STD, floating_model, interpreter, input_details, output_details)

detected_px_coordinates = []
# Loop over all detections and draw detection box if confidence is above minimum threshold
current_count = 0
for i in range(len(scores)):
    if ((scores[i] > MIN_CONF_THRESH) and (scores[i] <= 1.0)):
        coordinates = {}
        # Get bounding box coordinates and draw box
        # Interpreter can return coordinates that are outside of image dimensions, need to force them to be within image using max() and min()
        ymin_cropped = int(max(1,(boxes[i][0] * imH)))
        xmin_cropped = int(max(1,(boxes[i][1] * imW)))
        ymax_cropped = int(min(imH,(boxes[i][2] * imH)))
        xmax_cropped = int(min(imW,(boxes[i][3] * imW)))
        object_name = labels[int(classes[i])] # Look up object name from "labels" array using class index
        
        ymin = ymin_cropped + crop_coords['ymin']
        xmin = xmin_cropped + crop_coords['xmin']
        ymax = ymax_cropped + crop_coords['ymin']
        xmax = xmax_cropped + crop_coords['xmin']
        x_px = xmin + ((xmax-xmin)/2)
        y_px = ymin + ((ymax-ymin)/2)

        #draw center of detected object on original image, not cropped image, and store these values
        undistorted_roi = cv2.circle(undistorted_roi, (int(x_px), int(y_px)), radius=5, color=(0, 0, 255), thickness=-10) 
        coordinates[str(object_name)] = [x_px, y_px]
        detected_px_coordinates.append(coordinates)

        # Draw label
        label = '%s: %d%%' % (object_name, int(scores[i]*100))
        labelSize, baseLine = cv2.getTextSize(label, cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.7, 2) #Get font size
        label_ymin = max(ymin, labelSize[1] + 10) #Make sure not to draw label too close to top of window
        if object_name == 'correct':
            cv2.rectangle(undistorted_roi, (xmin,ymin), (xmax,ymax), (10, 255, 0), 2)
            cv2.rectangle(undistorted_roi, (xmin, label_ymin-labelSize[1]-10), (xmin+labelSize[0], label_ymin+baseLine-10), (0, 255, 0), cv2.FILLED) #Draw box to put label text in
            cv2.putText(undistorted_roi, label, (xmin, label_ymin-7), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.7, (0, 0, 0), 1) #Draw label text

        elif object_name == 'side':
            cv2.rectangle(undistorted_roi, (xmin,ymin), (xmax,ymax), (255, 255, 0), 2)
            cv2.rectangle(undistorted_roi, (xmin, label_ymin-labelSize[1]-10), (xmin+labelSize[0], label_ymin+baseLine-10), (255, 255, 0), cv2.FILLED) #Draw box to put label text in
            cv2.putText(undistorted_roi, label, (xmin, label_ymin-7), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.7, (0, 0, 0), 1) #Draw label text

        else:
            cv2.rectangle(undistorted_roi, (xmin,ymin), (xmax,ymax), (255, 153, 255), 2)
            cv2.rectangle(undistorted_roi, (xmin, label_ymin-labelSize[1]-10), (xmin+labelSize[0], label_ymin+baseLine-10), (255, 153, 255), cv2.FILLED) #Draw box to put label text in
            cv2.putText(undistorted_roi, label, (xmin, label_ymin-7), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.7, (0, 0, 0), 1) #Draw label text
            
        current_count+=1

cv2.putText (undistorted_roi,'Total Detection Count : ' + str(current_count),(15,65),cv2.FONT_HERSHEY_SIMPLEX,1,(0,255,55),2,cv2.LINE_AA)

cameraXYZ = real_xyz.RealXYZ()
txt_file_name = "detected_objects.txt"
with open(txt_file_name, "w") as file_temp:
    for obj in detected_px_coordinates:
        obj_key = list(obj.keys())[0]
        u = obj[obj_key][0]
        v = obj[obj_key][1]
        cXYZ=cameraXYZ.calculate_XYZ(u, v)
        x_cm = cXYZ[0][0]
        y_cm = cXYZ[1][0]
        z_cm = cXYZ[2][0]
        print('coordinates: ', x_cm, ', ', y_cm)
        str_data = "pose: {}, coordinates in cm (x, y): [{}, {}] \n".format(obj_key, x_cm, y_cm)
        file_temp.write(str_data)

file_temp.close()

while(1):
    cv2.namedWindow('cropped', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('cropped', 1080, 720)
    cv2.imshow('cropped', undistorted_roi_cropped)
    
    cv2.namedWindow('Detections', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Detections', 1080, 720)
    cv2.imshow('Detections', undistorted_roi)
    k=cv2.waitKey(5)
    if k==27:
        break

cv2.destroyAllWindows()

print(detected_px_coordinates)
print("1st inference time: ", inference_time, " seconds")

print("second inference time: ", inference_time2, " seconds")
print("total inference time: ", inference_time + inference_time2, " seconds")
print("Done")


