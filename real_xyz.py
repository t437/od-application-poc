import numpy as np

class RealXYZ:
    def __init__(self):
        savedir="camera_calib_settings/"
        self.s_arr         = np.load(savedir+'s_arr.npy')
        self.newcam_mtx    = np.load(savedir+'newcam_mtx.npy')
        self.tvec1         = np.load(savedir+'tvec1.npy')
        self.R_mtx         = np.load(savedir+'R_mtx.npy')
        self.scalingfactor = self.s_arr[0]
        self.inverse_R_mtx = np.linalg.inv(self.R_mtx)
        self.inverse_newcam_mtx = np.linalg.inv(self.newcam_mtx)


    def calculate_XYZ(self,u,v):
                                        
        #Solve: From Image Pixels, find World Points

        uv_1=np.array([[u,v,1]], dtype=np.float32)
        uv_1=uv_1.T
        suv_1=self.scalingfactor*uv_1
        xyz_c=self.inverse_newcam_mtx.dot(suv_1)
        xyz_c=xyz_c-self.tvec1
        XYZ=self.inverse_R_mtx.dot(xyz_c)

        return XYZ