import io
import cv2
import picamera
import numpy as np
import tflite_runtime.interpreter as tflite

import real_xyz

PATH_TO_MODEL_DIR = 'fine_tuned_model_ssd_mobilenet_v2_fpnlite_15ksteps_closeup/tflite/model.tflite'
PATH_TO_LABELS = 'tflite_label_map.txt'
MIN_CONF_THRESH = 0.25 # use 0.25

calib_settings_dir = "./camera_calib_settings/"
camera_matrix   = np.load(calib_settings_dir+'cam_mtx.npy')
dist            = np.load(calib_settings_dir+'dist.npy')
newcameramatrix = np.load(calib_settings_dir+'newcam_mtx.npy')
roi             = np.load(calib_settings_dir+'roi.npy')

interpreter = tflite.Interpreter(model_path=PATH_TO_MODEL_DIR)
with open(PATH_TO_LABELS, 'r') as f:
    labels = [line.strip() for line in f.readlines()]

interpreter.allocate_tensors()
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
height = input_details[0]['shape'][1]
width = input_details[0]['shape'][2]
floating_model = (input_details[0]['dtype'] == np.float32)
input_mean = 127.5
input_std = 127.5

stream = io.BytesIO()
with picamera.PiCamera() as camera:
    camera.resolution = (3280, 2464)
    camera.capture(stream, format='jpeg')

#Convert the picture into a numpy array
buff = np.frombuffer(stream.getvalue(), dtype=np.uint8)
#Now creates an OpenCV image
image = cv2.imdecode(buff, 1)

image_undst = cv2.undistort(image, camera_matrix, dist, None, newcameramatrix)
#crop image according to ROI determined by cv2.getOptimalNewCameraMatrix() (not in this code), here just imported the exported value
x, y, w, h = roi
undistorted_roi = image_undst[y:y+h, x:x+w]
print('Running inference ... ')

# Acquire frame and resize to expected shape [1xHxWx3]
image_rgb = cv2.cvtColor(undistorted_roi, cv2.COLOR_BGR2RGB)
imH, imW, _ = undistorted_roi.shape
image_resized = cv2.resize(image_rgb, (width, height))
input_data = np.expand_dims(image_resized, axis=0)

# Normalize pixel values if using a floating model (i.e. if model is non-quantized)
if floating_model:
    input_data = (np.float32(input_data) - input_mean) / input_std

# Perform the actual detection by running the model with the image as input
interpreter.set_tensor(input_details[0]['index'],input_data)
interpreter.invoke()

# Retrieve detection results
boxes = interpreter.get_tensor(output_details[1]['index'])[0] # Bounding box coordinates of detected objects
classes = interpreter.get_tensor(output_details[3]['index'])[0] # Class index of detected objects
scores = interpreter.get_tensor(output_details[0]['index'])[0] # Confidence of detected objects

detected_px_coordinates = []

# Loop over all detections and draw detection box if confidence is above minimum threshold
current_count = 0
for i in range(len(scores)):
    if ((scores[i] > MIN_CONF_THRESH) and (scores[i] <= 1.0)):
        coordinates = {}
        # Get bounding box coordinates and draw box
        # Interpreter can return coordinates that are outside of image dimensions, need to force them to be within image using max() and min()
        ymin = int(max(1,(boxes[i][0] * imH)))
        xmin = int(max(1,(boxes[i][1] * imW)))
        ymax = int(min(imH,(boxes[i][2] * imH)))
        xmax = int(min(imW,(boxes[i][3] * imW)))
        object_name = labels[int(classes[i])] # Look up object name from "labels" array using class index
        
        x_px = xmin + ((xmax-xmin)/2)
        y_px = ymin + ((ymax-ymin)/2)
        #draw center of detected object
        undistorted_roi = cv2.circle(undistorted_roi, (int(x_px), int(y_px)), radius=5, color=(0, 0, 255), thickness=-10)
        

        #coordinates[str(object_name)] = [xmin, ymin, xmax, ymax]
        coordinates[str(object_name)] = [x_px, y_px]
        detected_px_coordinates.append(coordinates)

        # Draw label
        label = '%s: %d%%' % (object_name, int(scores[i]*100))
        labelSize, baseLine = cv2.getTextSize(label, cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.7, 2) #Get font size
        label_ymin = max(ymin, labelSize[1] + 10) #Make sure not to draw label too close to top of window
        if object_name == 'correct':
            cv2.rectangle(undistorted_roi, (xmin,ymin), (xmax,ymax), (10, 255, 0), 2)
            cv2.rectangle(undistorted_roi, (xmin, label_ymin-labelSize[1]-10), (xmin+labelSize[0], label_ymin+baseLine-10), (0, 255, 0), cv2.FILLED) #Draw box to put label text in
            cv2.putText(undistorted_roi, label, (xmin, label_ymin-7), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.7, (0, 0, 0), 1) #Draw label text

        elif object_name == 'side':
            cv2.rectangle(undistorted_roi, (xmin,ymin), (xmax,ymax), (255, 255, 0), 2)
            cv2.rectangle(undistorted_roi, (xmin, label_ymin-labelSize[1]-10), (xmin+labelSize[0], label_ymin+baseLine-10), (255, 255, 0), cv2.FILLED) #Draw box to put label text in
            cv2.putText(undistorted_roi, label, (xmin, label_ymin-7), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.7, (0, 0, 0), 1) #Draw label text

        else:
            cv2.rectangle(undistorted_roi, (xmin,ymin), (xmax,ymax), (255, 153, 255), 2)
            cv2.rectangle(undistorted_roi, (xmin, label_ymin-labelSize[1]-10), (xmin+labelSize[0], label_ymin+baseLine-10), (255, 153, 255), cv2.FILLED) #Draw box to put label text in
            cv2.putText(undistorted_roi, label, (xmin, label_ymin-7), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.7, (0, 0, 0), 1) #Draw label text
            
        current_count+=1

cv2.putText (undistorted_roi,'Total Detection Count : ' + str(current_count),(15,65),cv2.FONT_HERSHEY_SIMPLEX,1,(0,255,55),2,cv2.LINE_AA)

resizing_factor = 2
height, width, channels = undistorted_roi.shape
height_resized = int(height/resizing_factor)
width_resized = int(width/resizing_factor)

cameraXYZ = real_xyz.RealXYZ()
txt_file_name = "detected_objects.txt"
with open(txt_file_name, "w") as file_temp:
    for obj in detected_px_coordinates:
        obj_key = list(obj.keys())[0]
        u = obj[obj_key][0]
        v = obj[obj_key][1]
        cXYZ=cameraXYZ.calculate_XYZ(u, v)
        x_cm = cXYZ[0][0]
        y_cm = cXYZ[1][0]
        z_cm = cXYZ[2][0]
        print('coordinates: ', x_cm, ', ', y_cm)
        str_data = "pose: {}, coordinates in cm (x, y): [{}, {}] \n".format(obj_key, x_cm, y_cm)
        file_temp.write(str_data)

file_temp.close()

while(1):
    cv2.namedWindow('Detections', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('Detections', height_resized, width_resized)
    cv2.resizeWindow('Detections', 1080, 720)
    cv2.imshow('Detections', undistorted_roi)
    k=cv2.waitKey(5)
    if k==27:
        break

cv2.destroyAllWindows()

print(detected_px_coordinates)
print("Done")
